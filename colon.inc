%define DICT_START 0  ; set initial element's label

%macro colon 2 ; macro for 2 arguments: key, label
    %ifnstr %1 ; check if 1st argument is a string
        %error "First argument was expected to be a string!"
    %endif
    %ifnid %2 ; check if 2nd argument is an identifier
        %error "Second argument was expected to be an identifier!"
    %endif 

    %2: dq DICT_START ; write a label
    %define DICT_START %2 ; change label for the next element
    db %1, 0 ; write a key
%endmacro
