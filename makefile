ASM=nasm
ASMFLAGS=-f elf64
LD=ld

.PHONY: clean

all: main 

clean:
	rm *.o

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm lib.inc words.inc colon.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

main: main.o lib.o dict.o
	$(LD) -o $@ $^
