%include "lib.inc"
%include "dict.inc"

section .bss                        ; uninitialized read-write data

%define BUFFER_SIZE 256
buffer: resb BUFFER_SIZE            ; 256 uninitialized bytes for each symbol

section .rodata                     ; read-only data

enter_key: db "Enter key: ", 0
value_by_key: db "Value by entered key: ", 0

err_empty_dict: db "The dictionary you're trying to reach is empty!", 0
err_no_such_key: db "There is no such key in dictionary!", 0
err_buf_overflow: db "The key is too long! (should be less than 256 symbols)", 0

%include "words.inc"

section .text

global _start

_start:
    mov rdi, DICT_START             ; mov address of dictionary to rdi
    cmp rdi, 0                      ; check if dictionary is empty
    je .err_empty_dict              ; if so - exit
    push rdi 

    mov rdi, enter_key              ; print "Enter key: "
    call print_string 

    mov rdi, buffer                 
    mov rsi, BUFFER_SIZE
    call read_string                ; results: rax - address, rdx - length of the string
    cmp rax, 0                      ; check if buffer is overflowed 
    je .err_buf_overflow            ; if so - exit

    mov rdi, rax                    
    pop rsi
    call find_word                  ; results: rax - address of the entry
    cmp rax, 0                      ; check if key doesn't exist  
    je .err_no_such_key             ; if so - exit

    push rax 
    mov rdi, value_by_key
    call print_string               ; print "Value by entered key: "
    pop rax

    lea rdi, [rax+8]                ; go to the key address from the entry
    push rdi        
    call string_length              ; results: rax - length of the string
    pop rdi                         
    lea rdi, [rdi+rax+1]            ; go to the actual label (+1 for null-terminated string)
    call print_string_newline
    xor rdi, rdi       
    jmp exit                        ; the end!

    .err_empty_dict:
        mov rdi, err_empty_dict
        call print_error_newline
        jmp exit
    .err_no_such_key:
        mov rdi, err_no_such_key
        call print_error_newline
        jmp exit
    .err_buf_overflow:
        mov rdi, err_buf_overflow
        call print_error_newline
        jmp exit




