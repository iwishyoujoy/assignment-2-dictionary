%include "lib.inc"

section .text

global find_word

find_word:                      ; rdi - string address, rsi - dictionary address
    push rbx                    
    mov rbx, rsi                ; set counter
    .loop:
        lea rsi, [rbx+8]        ; step from the label to key 
        call string_equals      
        test rax, rax           ; if strings are equal go to .found 
        jnz .found
        mov rbx, [rbx]          ; set counter for next element
        test rbx, rbx           ; check if is the end of dictionary
        jz .not_found
        jmp .loop
    .found:
        mov rax, rbx            ; return address of entry
        pop rbx
        ret
    .not_found:
        xor rax, rax            ; return 0
        pop rbx
        ret


