; library for all functions from 1st lab 
section .text

%define NEW_LINE 0xA
%define SPACE 0x20
%define TAB 0x9

global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_string_newline
global print_error_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy

; Terminates the current process
exit: 
    mov rax, 60                 ; 'exit' syscall number    
	syscall 

; Takes a pointer to null-terminated string and returns its length
string_length:
    xor rax, rax                ; to be sure that rax is zeroed or else it can be random
    .loop:                  
        cmp byte[rdi+rax], 0    ; check if the symbol - null-terminator 
        je .end                 ; if it is then go to the end
        inc rax                 ; if not then increase rax by 1 (go to the next symbol)
        jmp .loop               ; restart the loop
    .end:
        ret   

; Takes a pointer to null-terminated string and prints it to stdout
print_string:
    push rdi                    ; push the string sddress
    call string_length           
    mov rdx, rax                ; move rax (string length) to rdx
    pop rsi                     ; get the string address off the stack to rsi
    mov rax, 1                  ; system call number should be stored in rax
    mov rdi, 1                  ; move descriptor for stdout to rdi 
    syscall 
    ret

; Takes a pointer to null-terminated string and prints it to stderr
print_error:
    push rdi                    ; push the string sddress
    call string_length           
    mov rdx, rax                ; move rax (string length) to rdx
    pop rsi                     ; get the string address off the stack to rsi
    mov rax, 1                  ; system call number should be stored in rax
    mov rdi, 2                  ; move descriptor for stderr to rdi 
    syscall 
    ret

; Takes a character code and prints it to stdout
print_char:
    xor rax, rax                ; make sure rax is zeroed
    push rdi                    ; push the symbol address
    mov rsi, rsp                ; set pointer to the stack
    pop rdi                     ; clear the stack
    mov rax, 1                  ; 'write' syscall identifier
    mov rdi, 1                  ; stdout file descriptor
    mov rdx, 1                  ; write 1 byte
    syscall
    ret

; Creates a line break
print_newline:
    mov rdi, NEW_LINE       
    jmp print_char

; Takes a pointer to null-terminated string, prints it to stdout and creates a line break
print_string_newline: 
    call print_string
    call print_newline
    ret

; Takes a pointer to null-terminated string, prints it to stderr and creates a line break
print_error_newline:
    call print_error
    call print_newline
    ret

; Outputs an unsigned 8-byte number in decimal format
print_uint:
    xor rax, rax                ; make sure rax is zeroed
    mov rax, rdi                ; move the number address to rax
    mov r8, rsp                 ; save stack pointer
    mov r9, 10                  ; using 10 for dividing later
    push 0                      ; push the end of the line to the stack
    .loop:
        xor rdx, rdx            ; rdx = 0 
        div r9                  ; divide 1 digit
        add rdx, '0'            ; convert the number to ASCII
        dec rsp                 ; decrease stack pointer by 1
        mov byte[rsp], dl       ; lower byte of rdx goes to the stack
        cmp rax, 0              ; check if rax = 0 
        jne .loop               ; if not zero go to another loop
        mov rdi, rsp            ; save stack pointer (the start of the line)
        push r8                 ; save initial stack pointer 
        call print_string       ; print our number
        pop r8              
        mov rsp, r8             ; get back to initial stack pointer
        ret

; Outputs a signed 8-byte number in decimal format
print_int:
    xor rax, rax                ; make sure rax is zeroed 
    cmp rdi, 0                  ; compare number to 0 
    jge print_uint              ; if above or equal go to print_uint
    push rdi                    ; save number for later
    mov rdi, '-'                ; print '-' before the number
    call print_char            
    pop rdi                     ; move number back
    neg rdi                     ; make number true complement
    call print_uint
    ret

; Takes two pointers to null-terminated strings, returns 1 if they are equal, 0 otherwise
string_equals:                  ; 1 - rdi, 2 - rsi
    xor rax, rax                ; make sure everything is zeroed
    xor r8, r8                 
    xor r9, r9
    xor rcx, rcx                
    .loop:          
        mov r8b, byte[rdi+rcx]  ; put a symbol from the 1st string to r8
        mov r9b, byte[rsi+rcx]  ; put a symbol from the 2nd string to r9
        cmp r8, r9              ; compare symbols
        jne .not_equal          ; if they're not equal return 0
        cmp r8, 0               ; else check if the symbol is null-terminator
        je .equal               ; if so return 1
        inc rcx                 ; move to the next symbol
        jmp .loop               ; restart loop
    .equal:                 
        mov rax, 1
        ret
    .not_equal:
        mov rax, 0
        ret

; Reads one character from stdin and returns it. Returns 0 if the end of the stream has been reached
read_char:
    xor rax, rax                ; make sure rax is zeroed
    mov rdx, 1                  ; the amount of bytes
    mov rdi, 0                  ; stdin file descriptor
    push rdi                    ; dec stack pointer and push 0 on the top
    mov rsi, rsp                ; put buffer address to rsi
    syscall                 
    pop rax                     ; pop the char
    ret 

; Accepts: buffer address - rdi, buffer size - rsi.
; Reads a word from stdin into the buffer, skipping leading whitespace characters.
; Whitespace characters: space 0x20, tab 0x9 and newline 0xA.
; Stops and returns 0 if the word is too big for the buffer.
; On success, returns the address of the buffer in rax, the length of the word in rdx.
; Returns 0 to rax on failure.
; This function should add a null terminator to the word.
read_word:         
    xor rcx, rcx                ; counter for loop
    mov r8, 0                   ; length of the word
    .loop:
        push rdi                ; buffer address
        push rsi                ; buffer size
        push rcx                ; make sure read_char function doesn't change rcx 
        call read_char          ; read char from the word
        pop rcx                 ; get back saved rcx
        pop rsi                 ; get back saved buffer size
        pop rdi                 ; get back saved buffer address
        cmp rax, SPACE          ; check if char is a space
        je .skip                ; read next char
        cmp rax, TAB            ; check if char is \t
        je .skip
        cmp rax, NEW_LINE       ; check if char is \n
        je .skip
        cmp rax, 0              ; check if it's the end of the word
        je .done
        mov [rdi+rcx], rax      ; move char to the buffer
        inc rcx                 ; increase counter by 1 
        inc r8                  ; increase word length by 1
        cmp rcx, rsi            ; check if buffer is full
        jge .overflow           ; stop and return 0
        jmp .loop               ; repeat 
    .skip:
        cmp rcx, 0              ; check if it is a begining of the string
        jne .done               ; if not then the word is over go to .done
        jmp .loop               ; else repeat 
    .overflow:
        xor rax, rax            
        ret 
    .done: 
        xor rax, rax
        mov [rdi+rcx], rax      ; save 0 to the end
        mov rax, rdi  
        mov rdx, r8 
        ret
 
; Accepts: buffer address - rdi, buffer size - rsi.
; Reads a string from stdin into the buffer.
; Stops and returns 0 if the string is too big for the buffer.
; On success, returns the address of the buffer in rax, the length of the string in rdx.
; Returns 0 to rax on failure.
; This function should add a null-terminator to the string.
read_string:
    xor rcx, rcx 
    mov r8, 0                   ; length of the string 
    .loop:
        push rdi 
        push rsi 
        push rcx 
        call read_char 
        pop rcx
        pop rsi 
        pop rdi 
        cmp rax, NEW_LINE       ; check if char is \n 
        je .done 
        cmp rax, 0              ; check if it's the end of the string
        je .done                
        mov [rdi+rcx], rax      ; move char to buffer
        inc rcx                 ; increase counter by 1 
        inc r8                  ; increase string length by 1
        cmp rcx, rsi            ; check if buffer is full
        jge .overflow           ; stop and return 0
        jmp .loop               
    .done:
        xor rax, rax
        mov [rdi+rcx], rax      ; save 0 to the end
        mov rax, rdi            ; return address in rax
        mov rdx, r8             ; return length in rdx
        ret
    .overflow:
        xor rax, rax            ; return 0       
        ret 

; Takes a pointer to a string, tries to read an unsigned number from its beginning.
; Returns in rax: the number, in rdx : its length in characters.
; rdx = 0 if the number could not be read
parse_uint:
    xor rax, rax                ; number will be stored here
    xor rcx, rcx                ; save number length
    .loop:
        xor r8, r8 
        mov r8b, byte[rdi+rcx]  ; get symbol
        cmp r8b, '0'            ; check if it is below '0'
        jb .end                 
        cmp r8b, '9'            ; check if it is above '9'
        ja .end
        inc rcx                 ; increase length by 1
        sub r8b, '0'            ; remove zeroes
        imul rax, 10            ; multiply rax by 10
        add rax, r8             ; add to number new symbol
        jmp .loop
    .end:
        mov rdx, rcx
        ret

; Takes a pointer to a string, tries to read a signed number from its beginning.
; If there is a sign, spaces between it and the number are not allowed.
; Returns in rax: the number, rdx : its length in characters (including the sign, if any)
; rdx = 0 if the number could not be read
parse_int:
    cmp byte[rdi], '-'          ; check if number is negative 
    je .negative_number         ; if so go to .negative_number
    jmp parse_uint              ; else go to parse_uint
    .negative_number:
        inc rdi                 ; start parsing without '-'
        call parse_uint         ; parse the number
        inc rdx                 ; add extra 1 to length
        neg rax                 ; negative the result so the number will be two-complemented
        ret 

; It takes a pointer to a string, a pointer to a buffer, and a length of the buffer.
; Copies a string to a buffer
; Returns the length of the string if it fits in the buffer, otherwise 0
string_copy:                    ; 1 - rdi, 2 - rsi, 3 - rdx
    xor rax, rax                ; make sure rax is zeroed
    xor r9, r9                  ; for symbol of the string
    xor rcx, rcx                ; set counter for loop on 0
    .loop:
        mov r9b, byte[rdi+rcx]  ; save string symbol
        mov byte[rsi+rcx], r9b  ; save string symbol in buffer
        inc rcx                 ; increase counter by 1 
        cmp rcx, rdx
        jge .overflow
        cmp r9b, 0              ; check if it is the end of the string
        je .end                
        jmp .loop 
    .end: 
        mov rax, rcx             ; return string length
        ret
    .overflow:
        mov rax, 0
        ret 

